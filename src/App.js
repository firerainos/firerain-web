import React, { lazy, Suspense,Component} from 'react';
import './App.css';
import {Route, Switch} from 'react-router-dom'
import {CssBaseline} from "@material-ui/core";
import LoadingFrame from './widgets/LoadingFrame'

const Admin = lazy(() => import("./admin/Admin"));
const Login = lazy(() => import("./admin/Login"));
const Signup = lazy(() => import("./pages/Signup"));
const PageFrame = lazy(() => import('./pages/Frame'))

class App extends Component {

    render() {
        return (
            <div className="App">
                <CssBaseline />
                <Suspense fallback={<LoadingFrame/>}>
                <Switch>
                    <Route path="/admin" component={Admin}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/signup" component={Signup}/>
                    <Route path="/" component={PageFrame}/>
                </Switch>
                </Suspense>
            </div>
        );
    }
}

export default App;
