import React, { Component } from 'react'
import {
    Button,
    Typography,
    TextField,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions
} from '@material-ui/core'
import axios from 'axios'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import Tabs from '@material-ui/core/Tabs/Tabs'
import Tab from '@material-ui/core/Tab/Tab'
import Paper from '@material-ui/core/Paper/Paper'
import SwipeableViews from 'react-swipeable-views'

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        [theme.breakpoints.up('md')]: {
            padding: '5%',
        },
    },
    paper: {
        padding: 48,
        textAlign: 'left'
    },
    tabContainer: {
        textAlign: 'center',
        padding:48,
    },
    info: {
        textAlign: 'left'
    },
    isoList: {
        display: 'inline-flex',
        justifyContent: 'center',
        flexWrap: 'wrap'
    },
    isoItem: {
        paddingLeft:30,
        paddingRight:30,
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 400,
        },
    }
})

class Download extends Component {
    constructor (props, context) {
        super(props, context)
    }

    state = {
        value:1,
        open: false,
        region: '',
        email: '',
        qq: '',
        introduction: '',
        suggest: ''
    }

    handleClickOpen = () => {
        this.setState({open: true})
    }

    handleClose = () => {
        this.setState({open: false})
    }

    handleSubmit = () => {
        axios.post('/api/list', {
            region: this.state.region,
            email: this.state.email,
            qq: this.state.qq,
            introduction: this.state.introduction,
            suggest: this.state.suggest
        }).then(r => {
            if (r.data.code == 0) {
                this.setState({open: false})
                alert('提交成功')
            } else {
                alert('提交失败')
            }
        })
    }

    render () {
        const {classes} = this.props

        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <p>可以将镜像刻录到CD上，作为ISO文件安装，或者使用刻录工具(dd)直接写入U盘。它仅适用于新安装;现有的FireRainOS可以随时用pacman -Syu更新。或者如果您已经在运行Arch Linux，您可以添加我们的软件源并安装软件包</p>
                    <p>请记住，FireRain OS目前处于alpha阶段，可能存在问题。</p>
                <Tabs
                    value={this.state.value}
                    onChange={(event, value) => this.setState({value:value})}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                >
                    <Tab label="ARCH LINUX" />
                    <Tab label="镜像" />
                    <Tab label="测试版" />
                </Tabs>
                    <SwipeableViews
                        index={this.state.value}
                        onChangeIndex={(event, value) => this.setState({value:value})}
                        animateHeight={true}
                    >
                        <div className={classes.tabContainer}>

                        </div>
                        <div className={classes.tabContainer}>
                            <div className={classes.isoList}>
                                <div className={classes.isoItem}>
                                    <h4>FireRain Live ISO</h4>
                                    <Button variant="outlined" color="primary" size="large" onClick={()=> window.location.href = 'https://firerainos.gitlab.io/firerain-iso'}>下载</Button>
                                </div>
                                <div className={classes.isoItem}>
                                    <h4>FireRain Desktop ISO</h4>
                                    <Button variant="outlined" color="primary" size="large" onClick={()=> window.location.href = 'https://mega.nz/folder/7SwCiSqL#KN8OYSqI_FX5_x-_yc7l9w'}>公测版下载</Button>
                                </div>
                            </div>
                            <div className={classes.info}>
                                <p>信息</p>
                                <ul>
                                    <li>
                                        <a href="https://wiki.archlinux.org/index.php/USB_flash_installation_media_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)">制作USB启动器</a>
                                    </li>
                                    <li>
                                        <a href="https://wiki.archlinux.org/index.php/Install_from_existing_Linux_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)">从现有Linux安装</a>
                                    </li>
                                    <li>
                                        <a href="https://wiki.archlinux.org/index.php/Installation_guide_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)">FireRain Live ISO安装教程</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className={classes.tabContainer}>
                            <p>加入内测即可第一时间体验新版功能</p>
                            <Button variant="raised" color="primary" onClick={this.handleClickOpen}>内测资格申请</Button>
                        </div>
                    </SwipeableViews>
                </Paper>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">内测资格申请</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            <Typography variant="headline" component="h2">义务</Typography>
                            <p>1.积极参与内测活动</p>
                            <p>2.及时反馈最新的内测结果</p>
                            <p>3.积极与火雨操作系统官方开发者沟通问题</p>
                            <p>4.对火雨操作系统提出有意义的建议</p>
                            <p>5.不可私自散播内测活动的内容和数据</p>
                            <Typography variant="headline" component="h2">权利</Typography>
                            <p>1.在第一时间体验火雨操作系统的最新开发成果</p>
                            <p>2.提前获取最新的开发计划和动态</p>
                            <p>3.拥有特殊组别和更高的论坛权限</p>
                        </DialogContentText>
                        <TextField autoFocus required margin="normal" id="region" label="地区" type="text"
                                   onChange={(event) => this.setState({region: event.target.value})}
                                   defaultValue={this.state.region}
                                   fullWidth/>
                        <TextField autoFocus required margin="normal" id="email" label="邮箱" type="email"
                                   onChange={(event) => this.setState({email: event.target.value})}
                                   defaultValue={this.state.email}
                                   helperText="请使用常用邮箱，用于接收内部测试团队资格申请的处理结果(可能不会短时间内处理)" fullWidth/>
                        <TextField autoFocus required margin="normal" id="qq" label="QQ" type="text"
                                   onChange={(event) => this.setState({qq: event.target.value})}
                                   defaultValue={this.state.qq}
                                   helperText="用于及时沟通和加入FIreRain内测团队QQ群" fullWidth/>
                        <TextField
                            id="introduction"
                            label="自我介绍"
                            multiline
                            rowsMax="8"
                            margin="normal"
                            onChange={(event) => this.setState({introduction: event.target.value})}
                            defaultValue={this.state.introduction}
                            helperText="请填写 个人介绍+个人能力（电脑方面）"
                            fullWidth required
                        />
                        <TextField
                            id="suggest"
                            label="建议"
                            multiline
                            rowsMax="8"
                            margin="normal"
                            onChange={(event) => this.setState({suggest: event.target.value})}
                            defaultValue={this.state.suggest}
                            helperText="请发表您对内部测试团队的建议"
                            fullWidth required
                        />
                        <Typography variant="headline" component="h2">交流方式</Typography>
                        <p>1.FireRainOS内测团队QQ群</p>
                        <p>2.FireRainOS内测团队 Telegram 群</p>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            取消
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            提交
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

Download.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Download)
